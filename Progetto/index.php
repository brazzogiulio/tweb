<?php
session_start();
//require ("file_required.php");
include("html/top.html");
?>
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="js/index.js"></script>
</head>
    <body class="bg justify-content-center">
        <div class="d-flex text-center container-fluid justify-content-center">
            <div id="alert" class="w-50 mt-2 alert rounded-lg animated fadeInDown" role="alert"></div>
        </div>

        <div id="container_login_form" class="jumbotron container border rounded-lg shadow-lg text-center animated fadeInDown">
                <h2>Login page</h2>
                <form id="login_form" method="post">
                    <div class="form-group shadow-sm w-50 mx-auto">
                        <input type="text" name="username" class="form-control" placeholder="Username" autofocus required>
                    </div>
                    <div class="form-group shadow-sm w-50 mx-auto">
                        <input type="password" name="pwd" class="form-control" placeholder="Password" autofocus required>
                    </div>
                    <div class="container my-1">
                        <input id="btn_accedi" type="button" class="btn btn-primary" value="Accedi"/>
                    </div>
                </form>
                <form method="post">
                    <div class="container">
                        <input type="button" id="btn_show_register_form" class="btn btn-primary w-25" value="Registrati"></button>
                    </div>
                </form>
            </div>
        <div id="container_register_form" class="jumbotron container border rounded-lg shadow-lg text-center animated fadeInDown">
                    <h2>Register page</h2>
                    <form id="register_form" method="post">
                        <div class="form-group shadow-sm w-25 mx-auto">
                            <input type="text" name="username_register" class="form-control" placeholder="Username" autofocus required>
                        </div>
                        <div class="form-group shadow-sm w-50 mx-auto">
                            <input type="password" name="pwd_register" class="form-control" placeholder="Password" autofocus required>
                        </div>
                        <div class="form-group shadow-sm w-50 mx-auto">
                            <input type="password" name="pwd_register_confirm" class="form-control" placeholder="Confirm password" autofocus required>
                        </div>
                        <div class="container-fluid">
                            <input type="button" id="btn_register" class="btn btn-primary w-25 my-1" value="Registrati"/>
                        </div>
                        <input id="btn_back_login_form" class="btn btn-primary w-25" value="Indietro"/>
                    </form>
            </div>

<?php include("bottom.php"); ?>
