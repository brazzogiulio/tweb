<?php
/*
    ricava tutti i lavori
*/


    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $sql_num_campi = "SELECT * FROM works";


    if($result = mysqli_query($conn, $sql_num_campi)){
        $num_campo = array();
        $indirizzo_campo = array();
        $numero_lavoratori = array();
        $resultato = array();
        while($row = mysqli_fetch_array($result)){
            $num_campo[] = $row['num_campo'];
            $indirizzo_campo[] = $row['indirizzo_campo'];
            $numero_lavoratori[] = $row['numero_lavoratori'];
        }
        $resultato = array("status" => "success", "num_campo" => $num_campo, "indirizzo_campo" => $indirizzo_campo, "numero_lavoratori" => $numero_lavoratori);
    }else{
        $resultato = array("status" => "error");
    }

    echo json_encode($resultato, JSON_PRETTY_PRINT);
    mysqli_close($conn);
?>
