<?php
/*
    elimina il lavoro selezionato
*/
    session_start();
    include("../function.php");
    
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $num_campo = escape($_POST['delete_work']);
    $num_campo = mysqli_real_escape_string($conn, $num_campo);

    $sql = "DELETE FROM `works` WHERE `works`.`num_campo` = '$num_campo'";

    if ($result = mysqli_query($conn, $sql)) {
        $res = array('status' => 'success');
    } else {
        $res = array('status' => 'error');
    }

    echo json_encode($res, JSON_PRETTY_PRINT);
    mysqli_close($conn);
?>
