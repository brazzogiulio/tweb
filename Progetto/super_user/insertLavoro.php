<?php
/*
    inserisce il lavoro con il numero, l'indirizzo e il numero di lavoratori
*/

    include("../function.php");

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $num_campo = escape($_POST['num_campo']);
    $num_campo = mysqli_real_escape_string($conn, $num_campo);

    $indirizzo_campo = escape($_POST['indirizzo_campo']);
    $indirizzo_campo = mysqli_real_escape_string($conn, $indirizzo_campo);

    $num_lavoratori  = escape($_POST['num_lavoratori']);
    $num_lavoratori = mysqli_real_escape_string($conn, $num_lavoratori);

    $sql = "INSERT INTO `works` (`num_campo`, `indirizzo_campo`, `numero_lavoratori`) VALUES ('$num_campo', '$indirizzo_campo', '$num_lavoratori')";

    if(!empty($_POST['num_campo']) && !empty($_POST['indirizzo_campo']) && !empty($_POST['num_lavoratori'])){
        if (mysqli_query($conn, $sql)) {
            $msg['status'] = 'success';
        } else {
            $error = "Error: " . $sql . "<br>" . mysqli_error($conn);
            $msg['status'] = 'error';
        }
    }else{
        $msg['status'] = 'empty';
    }
    mysqli_close($conn);
    echo json_encode($msg, JSON_PRETTY_PRINT);

?>
