<?php
/*
Permette di eliminare il commento generato dall'utente
*/
    session_start();
    header('Content-type: application/json');

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $username = $_SESSION['username'];

    $sql="DELETE FROM `comments` WHERE `comments`.`name` = '$username'";

    if (mysqli_query($conn, $sql)) {
        $msg['status'] = 'success';
    } else {
        $error = "Error: " . $sql . "<br>" . mysqli_error($conn);
        $msg['status'] = 'error';
    }
    mysqli_close($conn);
    echo json_encode($msg, JSON_PRETTY_PRINT);

?>
