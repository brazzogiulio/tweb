<?php
/*
Ricava il commento generato dall'utente
*/
    session_start();
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }
    $sql = "SELECT name, text from comments";
    $result = mysqli_query($conn, $sql);
    $text = array();

    $name = $_SESSION['username'];

    while($row = mysqli_fetch_array($result)){
        if($row['name'] == $name){
            $msg = $row['text'];
            $text = array('text' => $msg);
            break;
        }
    }

    echo json_encode($text, JSON_PRETTY_PRINT);
    mysqli_close($conn);
?>
