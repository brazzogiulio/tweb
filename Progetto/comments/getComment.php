<?php
/*
Ricava tutti i commenti degli utenti
*/
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $sql_num_campi = "SELECT `name`,`text` FROM `comments`";


    $nome = array();
    $text = array();

    $resultato = array();

    if($result = mysqli_query($conn, $sql_num_campi)){
        while($row = mysqli_fetch_array($result)){
            $nome[] = $row['name'];
            $text[] = $row['text'];
            array_reverse($nome, true);
            array_reverse($text, true);
            //array_push($resultato, $nome, $text);
        }
        $resultato = array("name" => $nome, "text" => $text);
    }

    echo json_encode($resultato, JSON_PRETTY_PRINT);
    mysqli_free_result($result);
    mysqli_close($conn);
?>
