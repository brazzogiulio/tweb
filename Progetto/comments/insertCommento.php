<?php
/*
Inserisce nel database il commento dell'utente
*/
    session_start();
    include("../function.php");

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $username = escape($_SESSION['username']);
    $username = mysqli_real_escape_string($conn, $username);
    $text = escape($_POST['input_commento']);
    $text = mysqli_real_escape_string($conn, $text);


    if(!empty($_POST['input_commento'])){

        $sql="INSERT INTO `comments` (`commentiD`, `name`, `text`) VALUES (NULL, '$username', '$text')";

        if (mysqli_query($conn, $sql)) {
            $msg['status'] = 'success';
        } else {
            $error = "Error: " . $sql . "<br>" . mysqli_error($conn);
            $msg['status'] = 'error';
        }
    }else{
        $msg['status'] = 'empty';
    }

    echo json_encode($msg, JSON_PRETTY_PRINT);
    mysqli_close($conn);
?>
