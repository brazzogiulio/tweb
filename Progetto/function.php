<?php
    if(!isset($_SESSION)) {session_start();}

    function redirect($url) {
        header("Location: $url");
        die;
    }

    function escape($stringa){
        $stringa = str_replace("'", "", $stringa); #cancella il carattere ‘ dalla variabile
        $stringa = str_replace('"', "", $stringa); #cancella il carattere ” dalla variabile
        $stringa = htmlentities($stringa); #sostituisce i caratteri < e >
        return $stringa;
    }
?>
