<?php
    session_start();
    include("html/top.html");
    if(isset($_SESSION['username'])){

            include("html/super_user_page.html");
    }else{
        ?>
            <div class="text-center">
                <div class="container rounded-lg animated fadeInUp">
                    <div id="alert_logged" class="m-1 alert alert-danger show text-center animated" role="alert">
                        <?php
                            echo "Effettuare il login";
                        ?>
                    </div>
                </div>
                <form action="index.php" method="post">
                    <button id="btn_logout" type="submit" value="log_out" class="btn btn-primary m-3">Indietro</button>
                </form>
            </div>
        <?php
    }

?>
