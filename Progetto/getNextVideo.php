<?php
    $dir = 'video';
    $file = scandir($dir);
    $result = array();
    $index_next_video = 0;
    $reset = 0;
    for($i = 0; $i < sizeof($file); $i++){
        if(!is_dir($file[$i])){
            if($file[$i] == $_GET['current_video']) {
                $index_next_video = $i + 1;
                if($index_next_video == sizeof($file)){
                    $reset = 1;
                }
                break;
            }
        }
    }
    if($index_next_video != 0) {
        if($reset == 0) {
            $result = array("status" => "success", "next_video" => $file[$index_next_video], "reset" => $reset);
        }else{
            $result = array("status" => "success", "reset" => $reset);
        }
    }else{
        $result = array("status" => "error");
    }
    echo json_encode($result);
?>