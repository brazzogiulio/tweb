<?php
//setcookie($_SESSION['username'], $_SESSION['id']);
session_start();
//require("file_required.php");
include("html/top.html");
?>
    <link rel="stylesheet" type="text/css" href="../style/home_page_style.css">
    <script type="text/javascript" src="../js/home_page.js"></script>
</head>
<?php

if(isset($_SESSION['username'])){
    ?>
        <div class="rounded-lg animated fadeInUp ">
            <div id="alert_login" class="alert m-3 alert-success text-center animated fadeInDown" role="alert">
                <?php
                echo '<p class="message">Accesso effettuato.</p>';
                echo 'Benvenuto '. $_SESSION['username'];
                ?>
            </div>
        </div>
    <?php
    include("html/home_page.html");
?>
<?php
}else{
?>
    <div class="text-center">
        <div class="container rounded-lg animated fadeInUp delay-1s ">
            <div id="alert_logged" class="m-1 alert alert-danger show text-center animated" role="alert">
                <?php
                    echo "Effettuare il login";
                ?>
            </div>
        </div>
        <form action="index.php" method="post">
            <button id="btn_logout" type="submit" value="log_out" class="btn btn-primary m-3">Indietro</button>
        </form>
    </div>
<?php
}
?>
<?php
    include("bottom.php");
?>
