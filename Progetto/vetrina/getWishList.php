<?php
    /*
     * ricava dalla wish_list gli oggetti salvati
     */
    session_start();
    include("../function.php");
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $user_id = escape($_GET['id_user']);
    $user_id = mysqli_real_escape_string($conn, $user_id);


    $sql_num_campi = "SELECT id_oggetto, nome_oggetto, prezzo_oggetto, descrizione_oggetto
                        FROM inventario
                        JOIN wish_list
                        on inventario.id_oggetto = wish_list.id_item
                        WHERE id_user = '$user_id'";


    $id_oggetto = array();
    $nome_oggetto = array();
    $prezzo_oggetto = array();
    $descrizione_oggetto = array();

    if($result = mysqli_query($conn, $sql_num_campi)){
        while($row = mysqli_fetch_array($result)){
            $id_oggetto[] = $row['id_oggetto'];
            $nome_oggetto[] = $row['nome_oggetto'];
            $prezzo_oggetto[] = $row['prezzo_oggetto'];
            $descrizione_oggetto[] = $row['descrizione_oggetto'];
        }
        $resultato = array("status" => "success", "id_oggetto" => $id_oggetto, "nome_oggetto" => $nome_oggetto,
                            "prezzo_oggetto" => $prezzo_oggetto, "descrizione_oggetto" => $descrizione_oggetto);
    }else{
        $resultato = array("status" => "error");
    }

    echo json_encode($resultato, JSON_PRETTY_PRINT);
    mysqli_free_result($result);
    mysqli_close($conn);

?>
