<?php
/*
    ricava l'articolo dal database e restituisce:
    id, nome dell'oggetto, prezzo dell'oggetto, descrizione dell'oggetto
*/
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $sql_num_campi = "SELECT * FROM inventario";

    $id = array();
    $nome_oggetto = array();
    $prezzo_oggetto = array();
    $descrizione_oggetto = array();

    if($result = mysqli_query($conn, $sql_num_campi)){
        while($row = mysqli_fetch_array($result)){
            $id[] = $row['id_oggetto'];
            $nome_oggetto[] = $row['nome_oggetto'];
            $prezzo_oggetto[] = $row['prezzo_oggetto'];
            $descrizione_oggetto[] = $row['descrizione_oggetto'];
        }
        $resultato = array("status" => "success", "id_oggetto" => $id, "nome_oggetto" => $nome_oggetto,
                            "prezzo_oggetto" => $prezzo_oggetto, "descrizione_oggetto" => $descrizione_oggetto);
    }else{
        $resultato = array("status" => "error");
    }

    echo json_encode($resultato, JSON_PRETTY_PRINT);
    mysqli_free_result($result);
    mysqli_close($conn);





?>
