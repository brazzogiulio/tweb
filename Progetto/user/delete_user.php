<?php
/*
    elimina l'utente selezionato
*/
    session_start();
    include("../function.php");

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }


    $delete_username = escape($_POST['delete_username']);
    $delete_username = mysqli_real_escape_string($conn, $delete_username);


    $sql = "DELETE FROM `user` WHERE `user`.`username` = '$delete_username'";


    if ($result = mysqli_query($conn, $sql)) {
        session_regenerate_id(TRUE);
        $res = array('status' => 'success');
    } else {
        $res = array('status' => 'error');
    }

    echo json_encode($res, JSON_PRETTY_PRINT);
    mysqli_close($conn);
?>
