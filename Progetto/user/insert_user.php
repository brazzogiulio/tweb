<?php
/*
 * inserisce un nuovo utente
 */
    include ("../function.php");
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }


    $username = escape($_POST['username_register']);
    $username = mysqli_real_escape_string($conn, $username);

    $confirm_pwd = escape($_POST['pwd_register_confirm']);
    $confirm_pwd = mysqli_real_escape_string($conn, $confirm_pwd);

    $pwd_register = escape($_POST['pwd_register']);
    $pwd_register = mysqli_real_escape_string($conn, $pwd_register);


    $superuser = 0;

    $msg = array();


    if( !empty($_POST['username_register']) && !empty($_POST['pwd_register_confirm']) && !empty($_POST['pwd_register']) ){

        $hash_pwd = password_hash($pwd_register, PASSWORD_DEFAULT);

        $sql = "INSERT INTO `user` (`id`, `username`, `password`, `super_user`) VALUES (NULL, '$username', '$hash_pwd', '$superuser')";

        if($confirm_pwd == $pwd_register){
            if (mysqli_query($conn, $sql)) {
                $_SESSION['username'] = $username;
                $_SESSION['super_user'] = $superuser;
                $super_user = $_SESSION['super_user'];
                $msg = array('status' => "success");
            } else {
                $msg = array('status' => 'username_error');
            }
        }else{
            $msg = array('status' => 'wrong_pwd');
        }
    }else{
        $msg = array('status' => 'empty');
    }


    echo json_encode($msg, JSON_PRETTY_PRINT);
    mysqli_close($conn);


?>
