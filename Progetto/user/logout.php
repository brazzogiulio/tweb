<?php
/*
	Esegue il logout
*/

    include ("../function.php");
    session_unset();
    session_destroy();
    session_start();
    $_SESSION['message_logout'] = "Logout effettuato correttamente";
    redirect("../index.php");
?>
