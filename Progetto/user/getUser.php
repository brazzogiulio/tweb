<?php
/*
 * ricava i dati dell'utente corrente
 */
    session_start();

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $username = $_SESSION['username'];

    $sql = "SELECT id, username, super_user FROM user WHERE username = '$username'";


    if($result = mysqli_query($conn, $sql)){
        $row = mysqli_fetch_array($result);

        $username = $row['username'];
        $id = $row['id'];
        $super_user = $row['super_user'];

        $res = array('status' => 'success', 'username' => $username, 'id' => $id, 'super_user' => $super_user);
    }else{
        $res = array('status' => 'error');
    }

    echo json_encode($res, JSON_PRETTY_PRINT);
    mysqli_free_result($result);
    mysqli_close($conn);
?>
