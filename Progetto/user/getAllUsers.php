<?php
/*
 * ricava l'username, super_user di tutti gli utenti
*/

    session_start();

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $sql = "SELECT * FROM user";


    if($result = mysqli_query($conn, $sql)){
        $res = array();
        while($row = mysqli_fetch_array($result)){
            $username[] = $row['username'];
            $super_user[] = $row['super_user'];
        }
        $res = array("username" => $username, "super_user" => $super_user, "status" => "success");
    }else{
        $res = array('success' => 'error');
    }

    echo json_encode($res, JSON_PRETTY_PRINT);
    mysqli_free_result($result);
    mysqli_close($conn);
?>
