
<?php
    session_start();
    include ("../function.php");

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $mysql_db = "login_data";
    $conn = mysqli_connect($dbhost,$dbuser,$dbpass, $mysql_db) or die('errore di connessione');

    $username = escape($_POST['username']);
    $username = mysqli_real_escape_string($conn, $username);


    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    if(!empty($_POST['username'])){
        $sql = "SELECT * FROM user WHERE username = '$username'";

        if($result = mysqli_query($conn, $sql)){

            $riga = mysqli_fetch_array($result);

            $hash_pwd = $riga['password'];

            if( password_verify($_POST['pwd'], $hash_pwd))
            {
                $_SESSION['id'] = $riga['id'];
                $_SESSION['username'] = $username;
                session_regenerate_id(TRUE);
                $msg = array('status' => 'success');
            }else{
                $msg = array('status' => 'error');
            }
        }else{
            $msg = array('status' => 'error');
        }
    }else{
        $msg = array('status' => 'empty');
    }



    echo json_encode($msg, JSON_PRETTY_PRINT);
    mysqli_close($conn);
?>
