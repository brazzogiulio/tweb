<?php
    session_start();
    include("html/top.html"); //apre head
    if(isset($_SESSION['username'])){

        include("html/vetrina.html"); //apre body, chiude head
        include("bottom.php"); //chiude body
    }else{
        ?>
            <div class="text-center">
                <div class="container rounded-lg animated fadeInUp delay-1s ">
                    <div id="alert_logged" class="m-1 alert alert-danger show text-center animated" role="alert">
                        <?php
                            echo "Effettuare il login";
                        ?>
                    </div>
                </div>
                <form action="index.php" method="post">
                    <button id="btn_logout" type="submit" value="log_out" class="btn btn-primary m-3">Indietro</button>
                </form>
            </div>
        <?php
    }
?>
