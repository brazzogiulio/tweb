//gestisce tutta la home_page
$(document).ready(function() {
    var popup_login = sessionStorage.getItem("popup_login");
    $("#alert_login").hide();
    if (popup_login !== "true") {
        console.log(popup_login);
        sessionStorage.setItem("popup_login", "true");
        $("#alert_login").show();
        $("#alert_login").addClass("fadeOutUp delay-4s");
    } else {
        remove_notify();
    }


    updateComments();
    getUser();

    //ricava dal json le definizioni della pagina principale
    $.getJSON('txt.json', function (json) {
        $("#firstTxt").html(json.firstTxt);
        $("#secondTxt").html(json.secondTxt);
        $("#thirdTxt").html(json.thirdTxt);
        $("#indirizzo_mail").html(json.indirizzo_mail);
        $("#numero_telefono").html(json.numero_telefono);
        $("#fax").html(json.fax);
    });

    //handler del bottone commenta
    $("#btn_insert_commento").on('click', function () {
        $("#form_comments_after")[0].reset();
        remove_notify();
        insertComment();
    });

    //handler del bottone rimuovi commento
    $("#btn_remove_commento").on('click', function () {
        removeComment();

    });

    //handler del bottone aggiorna commento
    $("#btn_update_commento").on('click', function () {
        updatePersonalComment();
    });

    var current_video = "case.mp4";
    var video_player = $("#video_player");
    video_player.attr('src', "video/"+current_video);


    video_player.on('ended', function(){
        $.ajax({
            dataType: "json",
            url: "getNextVideo.php",
            method: "GET",
            data: {
                current_video: current_video
            },
            success: function (data) {
                if (data.status === "success") {
                    var video = data.next_video;
                    if(data.reset === 0){
                        current_video = video;
                    }else{
                        current_video = "case.mp4";
                    }
                    video_player.removeClass("animated fadeInRight");
                    video_player.addClass("animated fadeOutLeft");

                    setTimeout(function(){
                        video_player.attr('src', "video/"+current_video);
                        video_player.removeClass("animated fadeOutLeft");
                        video_player.addClass("animated fadeInRight");
                    }, 1000);

                }else{
                    alert("C'è stato un errore");
                }
            },
            error: function () {
                alert("C'è stato un errore");
            }
        });
    });

});
//ricava i dati dell'utente dal database
function getUser(){
    $.ajax({
        dataType: "json",
        url: "/user/getUser.php",
        method: "GET",
        success: function(data){
            if(data.super_user === '1'){
                $("#btn_my_page").css('display', 'inherit');
            }else{
                $("#btn_my_page").css('display', 'none');
            }
        },
        error: function(data){
            alert("errore di esecuzione");
        }
    });
}


//mostra la notifica con il messaggio msg
function notify(msg, success, alert){
    $("#alert").removeClass("alert-success alert-danger alert-warning");
    $("#alert").show();
    $("#alert").html(msg);
    $("#alert").addClass(success);
}


//rimuove la notifica
function remove_notify(){
    $("#alert").hide();
    $("#alert_login").hide();
}


//inserisce il commento dell'utente
function insertComment(){
    console.log($("#form_comments").serialize());
    $.ajax({
        url: "../comments/insertCommento.php",
        method: "POST",
        data: $("#form_comments").serialize(),
        dataType: "json",
        success: function(data){
            if(data.status === 'success'){
                notify("Commento inserito correttamente", "alert-success");
                $("#form_comments").show();
                console.log(data.text);
            }else if(data.status === 'error'){
                console.log("fail-error");
                notify("Errore nell'inserimento del commento.", "alert-danger");
            }else if(data.status === 'empty'){
                console.log("fail-empty");
                notify("Compila prima il campo.", "alert-warning");
            }
        },
        error: function(data) {
            alert('errore esecuzione');
        }
    }).done (function(){
        updateComments();
    });
}

//aggiorna il commento dell'utente
function updateComments(){
    //aggiorna i commenti di tutti gli utenti
    $.ajax({
        type: "GET",
        url: "/comments/getComment.php",
        dataType: "json",
        success: function(data){
            var nomi = data.name;
            var testi = data.text;
            nomi.reverse();
            testi.reverse();
            $("#put_comment").empty();
            for(var x = 0; x < nomi.length; x++){
                $("#put_comment").append(
                    '<div id="container_comment" class="col-md-3 rounded-lg shadow-lg border m-3 text-center">' +
                    '<h2 id="nome_commento" class="p-1" type="text" name="nome_commento">' +
                    '<strong>'+nomi[x]+'</strong></h2>' +
                    '<h3 id="text_commento" class="p-1" type="text" name="text_commento">"'+testi[x]+'"</h3>' +
                    '</div>');
            }
        },
        error: function() {
            alert('errore esecuzione');
        }
    });

    //aggiorna il commento dell'utente corrente
    $.ajax({
        type: "GET",
        url: "/comments/getPersonalComment.php",
        dataType: "json",
        success: function(data){
            if(data.text !== undefined){
                console.log(data.text);
                $("#form_comments").css('display', 'none');
                $("#form_comments_after").css('display', 'inherit');
                $('#text_comment_after').attr('value', data.text);
            }else{
                $("#form_comments_after").css('display', 'none');
                $("#form_comments").css('display', 'initial');
                $("#form_comments").get(0).reset();
            }
        },
        error: function() {
            alert('errore esecuzione');
        }
    });
}

//rimuove il commento dell'utente
function removeComment(){
    $.ajax({
        type: "POST",
        url: "/comments/deletePersonalComment.php",
        dataType: "json",
        success: function(data){
            notify("Commento rimosso.", "alert-success");
        },
        error: function() {
            alert('errore esecuzione');
        }
    }).done (function(){
        updateComments();
        $("#form_comments")[0].reset();
    });
}

//aggiorna il commento già presente dell'utente
function updatePersonalComment(){
    var text = document.getElementById('text_comment_after').value;
    $.ajax({
        type: "POST",
        url: "/comments/updatePersonalComment.php",
        dataType: "json",
        data: {
            text: text
        },
        success: function(data){
            console.log("Commento aggiornato.");
            $("#put_comment").empty();
            notify("Commento correttamente aggiornato", "alert-success");

        },
        error: function() {
            alert('errore esecuzione');
        }
    }).done (function(){
        updateComments();
    });
}
