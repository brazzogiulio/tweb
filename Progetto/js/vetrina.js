var user_id;
var btns_clicked = [];

//gestisce la pagina vetrina
$(document).ready(function(){
    check_super_user();
    getUser();

    //handler del bottone visualizza articoli
    var current_tab = $("#section_visualizza_articoli");
    $('ul.navbar-nav > li #btn_visualizza_articoli').click(function() {
        if(current_tab.selector !== $("#section_visualizza_articoli").selector){
            current_tab.css("display", "none");
            current_tab = $("#section_visualizza_articoli");
            current_tab.css("display", "initial");
        }
    });

    //handler del bottone inserisci articoli
    $('ul.navbar-nav > li #btn_inserisci_articolo').click(function(){
        if(current_tab.selector !== $("#section_inserisci_articoli").selector){
            current_tab.css("display", "none");
            current_tab = $("#section_inserisci_articoli");
            current_tab.css("display", "inherit");
        }
    });

    //handler del bottone wish_list
    $('#btn_show_wish_list').on('click', function(){
        current_tab.css('display', 'none');
        current_tab = $('#section_wish_list');
        current_tab.css('display', 'inherit');
        show_wish_list();
    });
});

//restituisce l'id dell'utente
function getUser(){
    $.ajax({
        dataType: "json",
        method: "GET",
        url: "../user/getUser.php",
        success: function(data){
            if(data.status === "success"){
                user_id = data.id;
            }else{
                alert("ERRORE DI ESECUZIONE");
            }

        },
        error: function(){
            alert("C'è stato un errore");
        }
    }).done (function(){
        updateArrayWishList(user_id);
    });
}

//mostra la wish_list
function show_wish_list(){
    $.ajax({
        dataType: "json",
        url: "../vetrina/getWishList.php",
        method: "GET",
        data:{
            id_user: user_id
        },
        success: function(data){
            if(data.status === "success"){
                $("#article_wish_list").empty();
                if(data.id_oggetto.length !== 0){
                    var id = data.id_oggetto;
                    var nome_oggetto = data.nome_oggetto;
                    var prezzo_oggetto = data.prezzo_oggetto;
                    var descrizione_oggetto = data.descrizione_oggetto;
                    for(var x = 0; x < id.length; x++){
                        $("#article_wish_list").append(
                        '<div class="justify-content-center">\
                            <div class="col m-3">\
                                <div class="card">\
                                    <img class="card-img-top" src="../img/article/'+nome_oggetto[x]+'.jpg">\
                                    <div class="card-body">\
                                        <h3 id="nome_oggetto" class="p-1 col card-title" maxlength="100" type="text" name="nome_commento"><strong>'+nome_oggetto[x]+'</strong></h3>\
                                        <h5 id="prezzo_oggetto" class="p-1 col card-text" type="text" name="text_commento">"'+prezzo_oggetto[x]+'"</h5>\
                                        <h5 id="descrizione_oggetto" class="p-1 col card-text" maxlength="100" type="text" name="text_commento">"'+descrizione_oggetto[x]+'"</h5>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>');
                    }
                }else{

                    $("#article_wish_list").append('\
                    <div class="border shadow-lg rounded-lg p-3">\
                        <h2 class="p-3"> Nessun articolo ancora presente nella wish list <h2>\
                    </div>\
                    ')
                }
            }else{
                alert("ERRORE NELLA QUERY");
            }
        },
        error: function(){
            alert("C'è stato un erorre");
        }
    });
}

//mostra tutti i lavori già svolti nella section_visualizza_articoli
function showArticle(){
    $.ajax({
        type: "GET",
        url: "../vetrina/getArticles.php",
        dataType: "json",
        success: function(data){
            var id = data.id_oggetto;
            var nome_oggetto = data.nome_oggetto;
            var prezzo_oggetto = data.prezzo_oggetto;
            var descrizione_oggetto = data.descrizione_oggetto;
            for(var x = 0; x < id.length; x++){
                var aria_pressed;
                if(btns_clicked.includes(id[x])){
                    aria_pressed = "active";
                }else{
                    aria_pressed = "";
                }

                $("#article").append(
                '<div class="justify-content-center">\
                    <div class="col m-3">\
                        <div class="card">\
                            <button id="btn_save_'+id[x]+'" type="button" data-toggle="button" class="'+aria_pressed+' btn btn-info m-3 position-absolute" value="btn_save">Salva</button>\
                            <img class="card-img-top" src="../img/article/'+nome_oggetto[x]+'.jpg">\
                            <div class="card-body">\
                                <h3 id="nome_oggetto" class="p-1 col card-title" maxlength="100" type="text" name="nome_commento"><strong>'+nome_oggetto[x]+'</strong></h3>\
                                <h5 id="prezzo_oggetto" class="p-1 col card-text" type="text" name="text_commento">"'+prezzo_oggetto[x]+'"</h5>\
                                <h5 id="descrizione_oggetto" class="p-1 col card-text" maxlength="100" type="text" name="text_commento">"'+descrizione_oggetto[x]+'"</h5>\
                            </div>\
                        </div>\
                    </div>\
                </div>');
                $("#btn_save_"+id[x]).on('click', function(e){
                    btn_clicked(e.target.id);
                });
            }
        },
        error: function(data){
            alert("errore di esecuzione");
        }
    });
}

//controlla se l'utente è super_user,
//mostrando la possibilità di inserire articoli in caso affermativo
function check_super_user(){
    $.ajax({
        dataType: "json",
        url: "/user/getUser.php",
        method: "GET",
        success: function(data){
            if(data.super_user === 1){
                $("#btn_inserisci_articolo").css('display', 'run-in');
            }else{
                $("#btn_inserisci_articolo").css('display', 'none');
            }
        },
        error: function(data){
            alert("errore di esecuzione");
        }
    });
}

//salva nell'array locale gli articoli già presenti in wish_list
function updateArrayWishList(){
    $.ajax({
        dataType: "json",
        url: "../vetrina/getWishList.php",
        method: "GET",
        data:{
            id_user: user_id
        },
        success: function(data){
            if(data.status === "success"){
                btns_clicked = data.id_oggetto;
            }else{
                alert("Errore nella query");
            }
        },
        error: function(data){
            alert("errore di esecuzione");
        }
    }).done(function(){
        showArticle();
    });
}

//Gestisce l'aggiunta o la rimozione di un articolo dalla wish_list
function btn_clicked(id_article){
    id_article = id_article.substring(9);
    //ho l'array btn_clicked che contiene gli articoli già salvati
    var exist = 0;
    if(btns_clicked.includes(id_article)){
        var index = btns_clicked.indexOf(id_article);
        btns_clicked.splice(index, 1);
        removeArticle(id_article);
    }else{
        btns_clicked.push(id_article);
        addArticle(id_article);

    }

}

//aggiunge articolo alla wish_list
function addArticle(id_article){
    $.ajax({
        dataType: "json",
        method: "POST",
        url: "../vetrina/save_article.php",
        data: {
            id_user: user_id,
            id_article: id_article
        },
        success: function(data){
            if(data.status === "success"){
                notify("Articolo salvato", "alert-success");
                setTimeout(function(){
                   remove_notify();
                },5000);
            }else{
                notify("Articolo non salvato", "alert-warning");
            }
        },
        error: function(){
            alert("C'è stato un errore");
        }
    });
}

//rimuove articolo alla wish_list
function removeArticle(id_article){
    $.ajax({
        dataType: "json",
        method: "POST",
        url: "../vetrina/delete_article.php",
        data: {
            id_user: user_id,
            id_article: id_article
        },
        success: function(data){
            if(data.status === "success"){
                notify("Articolo rimosso", "alert-success");
                setTimeout(function(){
                    remove_notify();
                }, 5000);
            } else{
                notify("Articolo non rimosso", "alert-warning");
            }
        },
        error: function(){
            alert("C'è stato un errore");
        }
    });
}

//notifica all'utente il messaggio msg e il tipo di alert
function notify(msg, success){
    console.log(msg);
    var alert = $("#alert");
    alert.removeClass("alert-success alert-danger alert-warning");
    alert.css("display", "inherit");
    alert.html(msg);
    alert.addClass(success);
}

//rimuove la notifica
function remove_notify(){
    var alert =  $("#alert");
    alert.css('display', 'none');
}