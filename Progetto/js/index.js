//gestisce la pagina di login
$(document).ready(function(){
    sessionStorage.setItem("popup_login", "false");

    //handler del bottone registrati
    $("#btn_show_register_form").on('click', function(){
        remove_notify();
        $("#register_form")[0].reset();
        showRegisterPage();
    });

    //handler del bottone indietro
    $("#btn_back_login_form").on('click', function(){
        $("#login_form")[0].reset();
        showLoginPage();
    });

    //handler del bottone register
    $("#btn_register").on('click', function(e){
        e.preventDefault();
        remove_notify();
        $.ajax({
            type: "POST",
            url: "../user/insert_user.php",
            dataType: "json",
            data: $('#register_form').serialize(),
            success: function(data) {
                if (data.status === 'success') {
                    console.log(data.username);
                    $("#login_form")[0].reset();
                    showLoginPage();
                    notify("Utente registrato correttamente", "alert-success");
                }else if(data.status === 'wrong_pwd'){
                    notify("Le password non corrispondo.", "alert-warning");
                }else{
                    notify("Nome utente già in uso.", "alert-warning");
                }
            },
            error: function(data){
                alert("errore di esecuzione");
            }
        });
    });

    //handler del bottone accedi
    $("#btn_accedi").on('click', function(e){
        $.ajax({
            type: "POST",
            url: "../user/verifica.php",
            dataType: "json",
            data: $('#login_form').serialize(),
            success: function(data){
                if(data.status === 'success'){
                    window.location.href = "../home_page.php";
                }else if(data.status === 'error'){
                    remove_notify();
                    notify("Credenziali non valide.", "alert-danger");
                }else if(data.status === "empty"){
                    remove_notify();
                    notify("Immettere delle credenziali valide.", "alert-warning");
                }
            },
            error: function(data){
                alert("errore di esecuzione");
            }
        });
    });

});

//mostra la pagina di login
function showLoginPage(){
    $("#container_register_form").css('display', 'none');
    $("#container_login_form").css('display', 'inherit');
}

//mostra la pagina di registrazione
function showRegisterPage(){
    $("#container_register_form").css('display', 'inherit');
    $("#container_login_form").css('display', 'none');
}

//mostra la notifica all'utente
function notify(msg, success){
    var alert = $("#alert");
    alert.removeClass("alert-success alert-danger alert-warning");
    alert.css("display", "initial");
    alert.html(msg);
    alert.addClass(success);
}

//rimuove la notifica
function remove_notify(){
    $("#alert").css("display", "none");
}
