//gestisce la pagina dell'amministratore
$(document).ready(function () {
    var current_tab = $("#section_visualizza_lavori");
    updateWorks();
    showUtenti();

    //handler del bottone visualizza lavori
    $('ul.navbar-nav > li #btn_lavori_correnti').click(function() {
        if(current_tab.selector != $("#section_visualizza_lavori").selector){
            remove_notify();
            current_tab.css("display", "none");
            current_tab = $("#section_visualizza_lavori");
            current_tab.css("display", "block");
            updateWorks();
        }
    });

    //handler del bottone modifica lavori
    $('ul.navbar-nav > li #btn_modifica_lavoro').click(function(){
        if(current_tab.selector != $("#section_modifica_lavori").selector){
            remove_notify();
            $("#my_form").get(0).reset();
            current_tab.css("display", "none");
            current_tab = $("#section_modifica_lavori");
            current_tab.css("display", "block");
        }
    });


    //handler del bottone gestisci utenti
    $('ul.navbar-nav > li #btn_gestisci_utenti').click(function(){
        if(current_tab.selector != $("#section_gestisci_utenti").selector){
            remove_notify();
            current_tab.css("display", "none");
            current_tab = $("#section_gestisci_utenti");
            current_tab.css("display", "block");
            showUtenti();
        }
    });

    //handler del bottone inserisci lavoro
    $("#btn_insert_lavoro").on('click', function(){
        remove_notify();
        console.log($("#my_form").serialize());
        $.ajax({
            url: "../super_user/insertLavoro.php",
            method: "POST",
            data: $("#my_form").serialize(),
            dataType: "json",
            success: function(data){
                if(data.status === 'success'){
                  notify("Campo inserito correttamente", "alert-success");
                }else if(data.status === 'error'){
                   notify("Inserimento del campo non riuscito.\nProbabilmente esiste già.", "alert-danger");
               }else if(data.status === 'empty'){
                   notify("Inserimento del campo non riuscito.\nCompila il modulo.", "alert-warning");
               }
            },
            error: function() {
                notify("C'è stato un problema", "alert-danger");
            }
        });
    });

    //handler del bottone elimina utente
    $("#btn_confirm_delete_user").on('click', function(){
        if(btns_clicked.length === 0){
            notify("C'è qualche errore", "alert-danger");
        }else{
            for(var x = 0; x < btns_clicked.length; x++){
                $.ajax({
                    dataType: "json",
                    method: "POST",
                    url: "../user/delete_user.php",
                    data: {
                        delete_username: btns_clicked[x]
                    },
                    success: function(data){
                        if(data.status === "success"){
                            notify("Utenti correttamente eliminati", "alert-success");
                            showUtenti();

                        }else{
                            console.log(data.status);
                            notify("C'è stato un errore con la connessione", "alert-danger");

                        }
                    },
                    error: function(data){
                        alert("errore di esecuzione");
                    }
                });
            }

        }
    });

    //handler del bottone conferma
    $("#btn_confirm_delete_works").on('click', function(){
        if(btns_clicked_works.length === 0){
            notify("C'è qualche errore", "alert-danger");
        }else{
            for(var x = 0; x < btns_clicked_works.length; x++){
                console.log(btns_clicked_works[x]);
                $.ajax({
                    dataType: "json",
                    method: "POST",
                    url: "../super_user/delete_lavoro.php",
                    data: {
                        delete_work: btns_clicked_works[x]
                    },
                    success: function(data){
                        if(data.status === "success"){
                            notify("Campi correttamente eliminati", "alert-success");
                            updateWorks();
                            $("#btn_confirm_delete_works").hide()
                        }else{
                            notify("C'è stato un errore con la connessione", "alert-danger");
                            $("#btn_confirm_delete_works").hide();
                        }
                    },
                    error: function(data){
                        alert("errore di esecuzione");
                    }
                });
            }
        }
    });

    /*
    $("#btn_search_works").on('click', function(){
        var input_search = document.getElementById("input_search").value;
        $.ajax({
            url: "../super_user/search_works.php",
            method: "POST",
            dataType: "json",
            data: {
                input_search: input_search
            },
            success: function(data){
                console.log(data.input_search);
                if(data.status == "success"){
                    console.log(data.num_campo);
                    if(data.num_campo.length != 0){
                        var campo = data.num_campo;
                        var indirizzo = data.indirizzo_campo;
                        var lavoratori = data.numero_lavoratori;
                        campo.reverse();
                        indirizzo.reverse();
                        lavoratori.reverse();
                        $("#works_start").empty();
                        for(var x = 0; x<campo.length; x++){
                            $("#works_start").append(
                            '<div id="row" class="row container-fluid justify-content-center">\
                                <div id="btn_container_'+campo[x]+'" class="col text-center">\
                                    <button id="btn_'+campo[x]+'" type="button" class="btn btn-info">'+campo[x]+'</button>\
                                </div>\
                                <div id="indirizzo_campo" class="col text-center w-25">'+indirizzo[x]+'</div>\
                                <div id="numero_lavoratori" class="col text-center">'+lavoratori[x]+'</div>\
                            </div><hr class="w-75">');
                            $("#btn_"+campo[x]).on('click', function(e){
                                handle_click_works(e.target.id);

                            });
                        }
                    }else{
                        $("#works_start").append('<div id="row" class="row container-fluid justify-content-center"><div id="numero_campo" class="col text-center"></div><div id="indirizzo_campo" class="col text-center w-25">'+"Ancora nessun lavoro"+'</div><div id="numero_lavoratori" class="col text-center"></div></div><hr class="w-75">');
                    }
                }else if(data.status == "error"){
                    notify("La ricerca non ha prodotto risultati.", "alert-warning")
                }else if(data.status == "empty"){
                    alert("campo vuoto");
                }
            },
            error: function(){
                alert("C'è stato un errore");
            }
        });
    });
    */
});

//mostra i lavori
function updateWorks(){
    $.ajax({
        type: "GET",
        url: "/super_user/getAllWorks.php",
        dataType: "json",
        success: function(data){
            if(data.num_campo.length !== 0){
                var campo = data.num_campo;
                var indirizzo = data.indirizzo_campo;
                var lavoratori = data.numero_lavoratori;
                campo.reverse();
                indirizzo.reverse();
                lavoratori.reverse();
                $("#works_start").empty();
                for(var x = 0; x<campo.length; x++){
                    $("#works_start").append(
                    '<div id="row" class="row container-fluid justify-content-center">\
                        <div id="btn_container_'+campo[x]+'" class="col text-center">\
                            <button id="btn_'+campo[x]+'" type="button" class="btn btn-info">'+campo[x]+'</button>\
                        </div>\
                        <div id="indirizzo_campo" class="col text-center w-25">'+indirizzo[x]+'</div>\
                        <div id="numero_lavoratori" class="col text-center">'+lavoratori[x]+'</div>\
                    </div><hr class="w-75">');
                    $("#btn_"+campo[x]).on('click', function(e){
                        console.log("click");
                        handle_click_works(e.target.id);
                    });
                }
            }else{
                $("#works_start").empty();
                $("#works_start").append('\
                <div id="row" class="row container-fluid justify-content-center">\
                <div id="numero_campo" class="col text-center"></div>\
                <div id="indirizzo_campo" class="col text-center w-25">'+"Ancora nessun lavoro"+'</div>\
                <div id="numero_lavoratori" class="col text-center"></div></div><hr class="w-75">');
            }
        },
        error: function() {
            alert('errore esecuzione');
        }
    });
    $("#btn_confirm_delete_works").hide();
}

//notifica all'utente il messaggio msg e il tipo di alert
function notify(msg, success){
    console.log(msg);
    $("#alert").removeClass("alert-success alert-danger alert-warning");
    $("#alert").css("display", "inherit");
    $("#alert").html(msg);
    $("#alert").addClass(success);
}

//rimuove la notifica
function remove_notify(){
    $("#alert").css("display", "none");
}

//mostra la section_gestisci_utenti, con tutti gli utenti
function showUtenti(){
    $.ajax({
        type: "GET",
        url: "../user/getAllUsers.php",
        dataType: "json",
        success: function(data){
            if(data.status === "success"){
                if(data.username !== 0){
                    var username = data.username;
                    var super_user = data.super_user;
                    $("#users_start").empty();
                    for(var x = 0; x < username.length; x++){
                        var c;
                        if(super_user[x] === 1){
                            c = "Sì"
                        }else{
                            c = "No"
                        }
                        $("#users_start").append('\
                        <div id="btns" class="row justify-content-center">\
                            <div id="btn_container_'+username[x]+'" class="col">\
                                <button id="btn_'+username[x]+'" type="button" class="btn btn-info">'+username[x]+'</button>\
                            </div>\
                            <div class="col">\
                                <div class="row">\
                                    <div class="col" id="user'+x+' ">'+c+'</div>\
                                </div>\
                            </div>\
                        </div>\
                        <hr class="w-75">');
                        $("#btn_"+username[x]).on('click', function(e){
                            handle_click_users(e.target.id);
                        });
                    }

                }else{
                    //$("#users_start").append('<div id="row" class="row container-fluid justify-content-center"><div id="numero_campo" class="col text-center"></div><div id="indirizzo_campo" class="col text-center w-25">'+"Ancora nessun utente"+'</div></div><hr class="w-75">');
                }
            }else{
                notify("C'è stato un errore", "alert-danger");
            }
        },
        error: function() {
            alert('errore esecuzione');
        }
    }).done (function(){
        $("#btn_confirm_delete_user").hide();
    });
}

//gestione del click su ogni utente
var btns_clicked = [];
function handle_click_users(username){
    user = username.substring(4);
    var exist = 0;
    for(var x = 0; x < btns_clicked.length; x++){
        if(btns_clicked.includes(user)){
            exist = 1;
            break;
        }
    }

    if(exist == 0){ //non c'è il bottone nell'array, lo aggiungo e metto delete
        btns_clicked.push(user);
        $("#btn_"+user).removeClass('btn-info');
        $("#btn_"+user).addClass('btn-danger');
        $("#btn_"+user).html("Elimina: " +user);

    }else{
        for(var x = 0; x < btns_clicked.length; x++){
            if(btns_clicked[x].includes(user) ) {
                btns_clicked.splice(x, 1);
                $("#btn_"+user).removeClass('btn-danger');
                $("#btn_"+user).addClass('btn-info');
                $("#btn_"+user).html(user);
                break;
            }
        }
    }
    if(btns_clicked.length === 0){
        $("#btn_confirm_delete_user").hide();
    }else{
        $("#btn_confirm_delete_user").show();
    }
}

//gestione del click su ogni numero di campo lavoro
var btns_clicked_works = [];
function handle_click_works(num_campo){
    num_campo = num_campo.substring(4);
    var exist = 0;
    for(var x = 0; x < btns_clicked_works.length; x++){
        if(btns_clicked_works.includes(num_campo)){
            exist = 1;
            break;
        }
    }

    if(exist === 0){ //non c'è il bottone nell'array, lo aggiungo e metto delete
        btns_clicked_works.push(num_campo);
        $("#btn_"+num_campo).removeClass('btn-info');
        $("#btn_"+num_campo).addClass('btn-danger');
        $("#btn_"+num_campo).html("Elimina: " +num_campo);

    }else{
        for(var x = 0; x < btns_clicked_works.length; x++){
            if(btns_clicked_works[x].includes(num_campo) ) {
                btns_clicked_works.splice(x, 1);
                $("#btn_"+num_campo).removeClass('btn-danger');
                $("#btn_"+num_campo).addClass('btn-info');
                $("#btn_"+num_campo).html(num_campo);
                break;
            }
        }
    }
    if(btns_clicked_works.length === 0){
        $("#btn_confirm_delete_works").hide();
    }else{
        $("#btn_confirm_delete_works").show();
    }
}
